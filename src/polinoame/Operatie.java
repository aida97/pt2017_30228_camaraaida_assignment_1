package polinoame;

public class Operatie {

	public static Polinom adunare(Polinom a, Polinom b) {
		a.sortMini();
		b.sortMini();
		Polinom result = new Polinom();
		int sa = a.getPolinom().size();
		int sb = b.getPolinom().size();
		int i = 0;
		int j = 0;
		while (i < sa && j < sb) {

			Monom monom = new Monom(0, 0);
			int put_a = a.getPolinom().get(i).getPutere();
			int put_b = b.getPolinom().get(j).getPutere();
			int cof_a = a.getPolinom().get(i).getCoef();
			int cof_b = b.getPolinom().get(j).getCoef();
			if (put_b > put_a) {
				j++;
				monom.setPutere(put_b);
				monom.setCoef(cof_b);
				result.getPolinom().add(monom);
			} else {
				if (put_a > put_b) {
					i++;
					monom.setPutere(put_a);
					monom.setCoef(cof_a);
					result.getPolinom().add(monom);
				} else {
					i++;
					j++;
					monom.setPutere(put_a);
					monom.setCoef(cof_a + cof_b);
					result.getPolinom().add(monom);
				}
			}
		}
		if (i < sa) {
			for (int k = i; k < sa; k++) {

				Monom monom = new Monom(0, 0);
				int put_a = a.getPolinom().get(k).getPutere();
				int cof_a = a.getPolinom().get(k).getCoef();
				monom.setPutere(put_a);
				monom.setCoef(cof_a);
				result.getPolinom().add(monom);
			}
		} else {
			if (j < sb)
				for (int k = j; k < sb; k++) {

					Monom monom = new Monom(0, 0);
					int put_b = b.getPolinom().get(k).getPutere();
					int cof_b = b.getPolinom().get(k).getCoef();
					monom.setPutere(put_b);
					monom.setCoef(cof_b);
					result.getPolinom().add(monom);
				}
		}

		return result;
	}

	public static Polinom scadere(Polinom a, Polinom b) {
		a.sortMini();
		b.sortMini();
		Polinom result = new Polinom();

		Polinom aux = new Polinom();
		for (Monom mon : b.getPolinom()) {
			int putere = mon.getPutere();
			int coef = -(mon.getCoef());
			Monom monAux = new Monom(coef, putere);
			aux.getPolinom().add(monAux);
		}
		result = adunare(a, aux);

		return result;
	}

	public static Polinom inmultire(Polinom a, Polinom b) {
		a.sortMini();
		b.sortMini();
		Polinom result = new Polinom();

		for (Monom ma : a.getPolinom()) {
			for (Monom mb : b.getPolinom()) {
				Monom monom = new Monom(0, 0);
				monom.setCoef(ma.getCoef() * mb.getCoef());
				monom.setPutere(ma.getPutere() + mb.getPutere());
				result.getPolinom().add(monom);
			}
		}
		result.sortMini();

		return result;
	}

	public static Polinom derivare(Polinom a) {
		Polinom result = new Polinom();

		for (Monom mon : a.getPolinom()) {

			Monom monom = new Monom(0, 0);
			if (mon.getPutere() != 0) {
				monom.setCoef(mon.getCoef() * mon.getPutere());
				monom.setPutere(mon.getPutere() - 1);
			}

			result.getPolinom().add(monom);
		}
		result.sortMini();
		return result;
	}

	public static Polinom integrare(Polinom a) {

		Polinom result = new Polinom();

		for (Monom mon : a.getPolinom()) {

			double coef = mon.getCoefD() / (double) (mon.getPutere() + 1);
			int putere = mon.getPutere() + 1;
			Monom monom = new Monom(coef, putere);
			result.getPolinom().add(monom);
		}
		result.sortMiniD();
		return result;
	}

	public static String afisare(Polinom a) {
		return a.toString();
	}
}
