package polinoame;

public class Service {

	public static String adunare(String pol1, String pol2) {

		Parser parse = new Parser();
		Polinom p1 = parse.resolve(pol1);
		Polinom p2 = parse.resolve(pol2);
		Polinom result = Operatie.adunare(p1, p2);
		return result.toString();
	}

	public static String scadere(String pol1, String pol2) {

		Parser parse = new Parser();
		Polinom p1 = parse.resolve(pol1);
		Polinom p2 = parse.resolve(pol2);
		Polinom result = Operatie.scadere(p1, p2);

		return result.toString();
	}

	public static String inmultire(String pol1, String pol2) {

		Parser parse = new Parser();
		Polinom p1 = parse.resolve(pol1);
		Polinom p2 = parse.resolve(pol2);
		Polinom result = Operatie.inmultire(p1, p2);

		return result.toString();
	}

	public static String derivare(String pol1) {

		Parser parse = new Parser();
		Polinom p1 = parse.resolve(pol1);

		Polinom result = Operatie.derivare(p1);

		return result.toString();
	}

	public static String integrare(String pol1) {

		Parser parse = new Parser();
		Polinom p1 = parse.resolve(pol1);

		Polinom result = Operatie.integrare(p1);

		return result.afis_double();
	}

}
