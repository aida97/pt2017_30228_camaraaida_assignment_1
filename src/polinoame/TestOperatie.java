package polinoame;

import static org.junit.Assert.*;

import org.junit.Before;

import org.junit.Test;

public class TestOperatie {

	private Polinom a;
	private Polinom b;
	private static final double DELTA = 1e-15;

	@Before
	public void setUp() throws Exception {

		a = new Polinom();
		b = new Polinom();
	}

	@Test
	public void testAdunare() {

		a.getPolinom().add(new Monom(5, 4));
		a.getPolinom().add(new Monom(4, 2));
		b.getPolinom().add(new Monom(3, 4));
		b.getPolinom().add(new Monom(2, 1));
		Polinom result = Operatie.adunare(a, b);
		assertEquals(result.getPolinom().get(0).getCoef(), 8);
		assertEquals(result.getPolinom().get(0).getPutere(), 4);

		assertEquals(result.getPolinom().get(1).getCoef(), 4);
		assertEquals(result.getPolinom().get(1).getPutere(), 2);

		assertEquals(result.getPolinom().get(2).getCoef(), 2);
		assertEquals(result.getPolinom().get(2).getPutere(), 1);

	}

	@Test
	public void testScadere() {

		a.getPolinom().add(new Monom(5, 4));
		a.getPolinom().add(new Monom(4, 2));

		b.getPolinom().add(new Monom(3, 4));
		b.getPolinom().add(new Monom(2, 1));
		Polinom result = Operatie.scadere(a, b);

		assertEquals(result.getPolinom().get(0).getCoef(), 2);
		assertEquals(result.getPolinom().get(0).getPutere(), 4);

		assertEquals(result.getPolinom().get(1).getCoef(), 4);
		assertEquals(result.getPolinom().get(1).getPutere(), 2);

		assertEquals(result.getPolinom().get(2).getCoef(), -2);
		assertEquals(result.getPolinom().get(2).getPutere(), 1);

	}

	@Test
	public void testInmultire() {

		a.getPolinom().add(new Monom(5, 4));
		a.getPolinom().add(new Monom(4, 2));

		b.getPolinom().add(new Monom(3, 4));
		b.getPolinom().add(new Monom(2, 1));
		Polinom result = Operatie.inmultire(a, b);

		assertEquals(result.getPolinom().get(0).getCoef(), 15);
		assertEquals(result.getPolinom().get(0).getPutere(), 8);

		assertEquals(result.getPolinom().get(1).getCoef(), 12);
		assertEquals(result.getPolinom().get(1).getPutere(), 6);

		assertEquals(result.getPolinom().get(2).getCoef(), 10);
		assertEquals(result.getPolinom().get(2).getPutere(), 5);

		assertEquals(result.getPolinom().get(3).getCoef(), 8);
		assertEquals(result.getPolinom().get(3).getPutere(), 3);

	}

	@Test
	public void testDerivare() {
		a.getPolinom().add(new Monom(5, 4));
		a.getPolinom().add(new Monom(4, 2));
		Polinom result = Operatie.derivare(a);

		assertEquals(result.getPolinom().get(0).getCoef(), 20);
		assertEquals(result.getPolinom().get(0).getPutere(), 3);

		assertEquals(result.getPolinom().get(1).getCoef(), 8);
		assertEquals(result.getPolinom().get(1).getPutere(), 1);
	}

	@Test
	public void testIntegrare() {
		a.getPolinom().add(new Monom(9, 4));
		a.getPolinom().add(new Monom(10, 3));
		Polinom result = Operatie.integrare(a);

		assertEquals(result.getPolinom().get(0).getCoefD(), 1.8, DELTA);
		assertEquals(result.getPolinom().get(0).getPutere(), 5);

		assertEquals(result.getPolinom().get(1).getCoefD(), 2.5, DELTA);
		assertEquals(result.getPolinom().get(1).getPutere(), 4);
	}

}
