package polinoame;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestParser {

	private String str;
	

	@Before
	public void setUp() throws Exception {

		str = "+5x^4-x^3+2x+5";

	}

	@Test
	public void testResolve() {
		Parser parse = new Parser();
		Polinom result = parse.resolve(str);
		assertEquals(result.getPolinom().get(0).getCoef(), 5);
		assertEquals(result.getPolinom().get(0).getPutere(), 4);

		assertEquals(result.getPolinom().get(1).getCoef(), -1);
		assertEquals(result.getPolinom().get(1).getPutere(), 3);

		assertEquals(result.getPolinom().get(2).getCoef(), 2);
		assertEquals(result.getPolinom().get(2).getPutere(), 1);

		assertEquals(result.getPolinom().get(3).getCoef(), 5);
		assertEquals(result.getPolinom().get(3).getPutere(), 0);
	}
 

}
