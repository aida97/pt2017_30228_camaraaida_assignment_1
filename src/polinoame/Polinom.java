package polinoame;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Polinom {

	private List<Monom> polinom = new ArrayList<Monom>();

	public List<Monom> getPolinom() {
		return polinom;
	}

	public String toString() {

		String result = new String();
		for (int i = 0; i < polinom.size(); i++) {
			int coef = polinom.get(i).getCoef();
			int putere = polinom.get(i).getPutere();
			if (coef > 0) {
				if (coef != 1) {
					if (putere != 0 && putere != 1) {
						result += "+" + Integer.toString(coef) + "x^" + Integer.toString(putere);
					} else if (putere == 0) {
						result += "+" + Integer.toString(coef);
					} else {
						result += "+" + Integer.toString(coef) + "x";
					}
				} else {
					if (putere != 0 && putere != 1) {
						result += "+x^" + Integer.toString(putere);
					} else if (putere == 0) {
						result += "+" + Integer.toString(coef);
					} else {
						result += "+x";
					}
				}

			} else if (coef < 0) {
				if (coef != -1) {
					if (putere != 0 && putere != 1) {
						result += Integer.toString(coef) + "x^" + Integer.toString(putere);
					} else if (putere == 0) {
						result += Integer.toString(coef);
					} else {
						result += Integer.toString(coef) + "x";
					}
				} else {
					if (putere != 0 && putere != 1) {
						result += "-x^" + Integer.toString(putere);
					} else if (putere == 0) {
						result += Integer.toString(coef);
					} else {
						result += "-x";
					}
				}

			}

		}

		return result;
	}

	public String afis_double() {

		String result = new String();
		for (int i = 0; i < polinom.size(); i++) {
			double coef = polinom.get(i).getCoefD();
			int putere = polinom.get(i).getPutere();
			if (coef > 0) {
				if (putere != 0 && putere != 1) {
					result += "+" + Double.toString(coef) + "x^" + Integer.toString(putere);
				} else if (putere == 0) {
					result += "+" + Double.toString(coef);
				} else {
					result += "+" + Double.toString(coef) + "x";
				}
			} else if (coef < 0) {

				result += Double.toString(coef) + "x^" + Integer.toString(putere);
			}

		}

		return result;
	}

	public void sortMini() {

		Collections.sort(polinom);
		for (int i = 0; i < polinom.size() - 1; i++) {

			if (polinom.get(i).getPutere() == polinom.get(i + 1).getPutere()) {
				int coef = polinom.get(i).getCoef() + polinom.get(i + 1).getCoef();
				int putere = polinom.get(i).getPutere();
				Monom monom = new Monom(coef, putere);

				polinom.add(i, monom);
				polinom.remove(i + 1);
				polinom.remove(i + 1);
				i--;

			}
		}
	}

	public void sortMiniD() {

		Collections.sort(polinom);
		for (int i = 0; i < polinom.size() - 1; i++) {

			if (polinom.get(i).getPutere() == polinom.get(i + 1).getPutere()) {
				double coef = polinom.get(i).getCoefD() + polinom.get(i + 1).getCoefD();
				int putere = polinom.get(i).getPutere();
				Monom monom = new Monom(coef, putere);

				polinom.add(i, monom);
				polinom.remove(i + 1);
				polinom.remove(i + 1);
				i--;

			}
		}
	}
}
