package polinoame;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controller {

	@FXML
	private TextField polinom1;
	@FXML
	private TextField polinom2;
	@FXML
	private TextArea rezultat;
	@FXML
	private URL location;
	@FXML
	private ResourceBundle resources;

	public Controller() {

	}

	@FXML
	private void initialize() {

	}

	@FXML
	private void adunare() {

		String pol1 = polinom1.getText();
		String pol2 = polinom2.getText();
		String result = Service.adunare(pol1, pol2);
		afisare(result);
	}

	@FXML
	private void scadere() {
		String pol1 = polinom1.getText();
		String pol2 = polinom2.getText();
		String result = Service.scadere(pol1, pol2);
		afisare(result);
	}

	@FXML
	private void inmultire() {
		String pol1 = polinom1.getText();
		String pol2 = polinom2.getText();
		String result = Service.inmultire(pol1, pol2);
		afisare(result);
	}

	@FXML
	private void derivare() {
		String pol1 = polinom1.getText();
		String result = Service.derivare(pol1);
		afisare(result);
	}

	@FXML
	private void integrare() {
		String pol1 = polinom1.getText();
		String result = Service.integrare(pol1);
		afisare(result);
	}

	@FXML
	private void afisare(String result) {
		rezultat.setText(result);
	}

}