package polinoame;

public class Monom implements Comparable<Monom> {

	private int coef;
	private int putere;
	private double coef_d;

	public Monom(int coef, int putere) {
		this.coef = coef;
		this.putere = putere;
		coef_d = coef;
	}
	
	public Monom(double coef, int putere) {
		this.putere = putere;
		coef_d = coef;
	}

	public void setCoef(int coef) {
		this.coef = coef;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public void setCoefD(double coef_d) {
		this.coef_d = coef_d;
	}

	public int getCoef() {
		return coef;
	}

	public int getPutere() {
		return putere;
	}

	public double getCoefD() {
		return coef_d;
	}

	public int compareTo(Monom other) {

		if (this.putere > other.getPutere()) {
			return -1;
		} else if (this.putere < other.getPutere()) {
			return 1;
		}
		return 0;
	}
}
