package polinoame;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

	public Polinom resolve(String p) {
		Polinom result = new Polinom();
		String regex = "(([+-]\\d+)[a-zA-Z]\\^(\\d+))|(([+-])[a-zA-z]\\^(\\d+))|(([+-]\\d+)[a-zA-Z])|(([+-])[a-zA-Z])|([+-]\\d+)";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(p);

		while (matcher.find()) {
			String monom_str = matcher.group();
			if (matcher.group().equals(matcher.group(1))) {
				int coef = Integer.parseInt(matcher.group(2));
				int putere = Integer.parseInt(matcher.group(3));
				result.getPolinom().add(new Monom(coef, putere));

			} else if (matcher.group().equals(matcher.group(4))) {
				char semn = monom_str.charAt(0);
				int coef;
				if (semn == '-') {
					coef = -1;
				} else {
					coef = 1;
				}
				int putere = Integer.parseInt(matcher.group(6));
				result.getPolinom().add(new Monom(coef, putere));

			} else if (matcher.group().equals(matcher.group(7))) {
				int coef = Integer.parseInt(matcher.group(8));
				int putere = 1;
				result.getPolinom().add(new Monom(coef, putere));

			} else if (matcher.group().equals(matcher.group(9))) {
				char semn = monom_str.charAt(0);
				int coef;
				if (semn == '-') {
					coef = -1;
				} else {
					coef = 1;
				}
				int putere = 1;
				result.getPolinom().add(new Monom(coef, putere));
			} else {
				int coef = Integer.parseInt(matcher.group(11));
				int putere = 0;
				result.getPolinom().add(new Monom(coef, putere));

			}

		}

		return result;
	}

}
